
CollisionDetector = {}

function CollisionDetector:isPointInObject(object, x, y)
	local s = math.sin(-object.rotation)
	local c = math.cos(-object.rotation)

	local distanceX = x-object:getCenterX()
	local distanceY = y-object:getCenterY()

	local newX = object:getCenterX() + distanceX * c - distanceY * s
  	local newY = object:getCenterY() + distanceX * s + distanceY * c
  	isInside =  newX >= object.x and newX <= (object.x + object.width) and newY >= object.y and newY <= (object.y + object.height)
	return isInside
end

function CollisionDetector:isPointInRect(x, y, rX1, rY1, rX2, rY2)
	return x >= rX1 and x <= rX2 and y >= rY1 and y <= rY2
end

function CollisionDetector:isClose(cx1, cy1, w1, h1, cx2, cy2, w2, h2)
	local vWidth = ((w1) ^ 2 + (h1) ^ 2) ^ 0.5 / 2
	local bWidth = ((w1) ^ 2 + (h2) ^ 2) ^ 0.5 / 2

	return math.dist(cx1, cy1, cx2, cy2) <= bWidth + vWidth
end

function CollisionDetector:isABBA(x1, y1, w1, h1, x2, y2, w2, h2)
	return ((math.abs(2 * (x1 - x2) + (w1 - w2)) < (w1 + w2)) and (math.abs(2 * (y1 - y2) + (h1 - h2)) < (h1 + h2)))
end

--Calculate the projection of a polygon on an axis
--and returns it as a [min, max] interval
--Vector axis, Polygon polygon,ref float min, ref float max
function CollisionDetector:projectPolygon(axis, polygon,min, max) 
    --To project a point on an axis use the dot product
    local dotProduct = polygon.points[1]:dotProduct(axis)
    local min = dotProduct
    local max = dotProduct
    for i = 1, #polygon.points do
    	dotProduct = polygon.points[i]:dotProduct(axis)
    	if dotProduct < min then
    	    min = dotProduct
    	elseif dotProduct > max then
    	    max = dotProduct
    	end
  	end
    return min,max
end

--Calculate the distance between [minA, maxA] and [minB, maxB]
--The distance will be negative if the intervals overlap
function CollisionDetector:getIntervalDistance(minA, maxA, minB, maxB)
    if minA < minB then
        return minB - maxA
    end
    return minA - maxB
end

-- Check if polygon A is going to collide with polygon B.
-- The last parameter is the *relative* velocity 
-- of the polygons (i.e. velocityA - velocityB)
function CollisionDetector:polygonCollision(polygonA, polygonB) 
    local edgeCount = #polygonA.edges + #polygonB.edges
    local edgeCountA = #polygonA.edges
    local edgeCountB = #polygonB.edges
    local minIntervalDistance = math.huge
    local translationAxis = Vector.new()
    local edge  = Vector.new()

   

    -- Loop through all the edges of both polygons
    for edgeIndex = 1, edgeCount do
        if edgeIndex <= edgeCountA then
            edge = polygonA.edges[edgeIndex]
        else 
            edge = polygonB.edges[edgeIndex - edgeCountA]
        end

        -- ===== 1. Find if the polygons are currently intersecting =====

        -- Find the axis perpendicular to the current edge
        axis =  Vector.new(-edge.x, edge.y)
        axis:normalize()

        -- Find the projection of the polygon on the current axis
        local minA, minB, maxA, maxB = 0
        minA, maxA = self:projectPolygon(axis, polygonA, minA, maxA);
        minB, maxB = self:projectPolygon(axis, polygonB, minB, maxB);

        -- Check if the polygon projections are currentlty intersecting
        if self:getIntervalDistance(minA, maxA, minB, maxB) > 0 then
            return false
        end
    end
    return true
end

function CollisionDetector:areObjectsColiding(object, objects) 
    for i = 1, #objects do
        if self:polygonCollision(object.polygon, objects[i].polygon) then
            return true
        end
    end
    return false
end

function CollisionDetector:areObjectsClose(objectA, objectB) 
    for i = 1, #objects do
        if self:polygonCollision(object.polygon, objects[i].polygon) then
            return true
        end
    end
    return false
end
