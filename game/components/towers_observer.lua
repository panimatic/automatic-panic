require 'vendor/class'

TowersObserver = class(function(o, data)
	o.enemies = data.enemies
	o.towers = data.towers
	o.towerGenerators = data.towerGenerators
	o.towers:setTowersObserver(o)
	o.towerGenerators:setTowersObserver(o)
end
)

function TowersObserver:update(tower)
	for key, item in ipairs(self.enemies.objects) do
		if (not item.isDead) and tower:isObjectInRange(item) then
		    tower:setEnemy(self.enemies.objects[key])
		    return
		end
	end
end
