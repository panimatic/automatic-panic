require 'vendor/class'
require 'components/collision_detector'
require 'extensions/math'

Viewport = class(function(o, x, y, width, height)
	o.x = x
	o.y = y
	o.width = width
	o.height = height
	o.offsetCenterX = 0
	o.offsetCenterY = 0
	o.offsetX = 0
	o.offsetY = 0
	o.canvas = love.graphics.newCanvas(width, height)
	o.lastCanvas = nil
end)

function Viewport:getOffsetBox()
	return self.offsetX, self.offsetY, self.width + self.offsetX, self.height + self.offsetY
end

function Viewport:isInside(x, y, width, height)
	return CollisionDetector:isABBA(x, y, width, height, self.offsetX, self.offsetY, self.width, self.height)
end

function Viewport:setOffset(ox, oy)
	self.offsetX = ox
	self.offsetY = oy
	self.offsetCenterX = (ox + self.width) / 2
	self.offsetCenterY = (oy + self.height) / 2
end

function Viewport:draw()
	love.graphics.draw(self.canvas, self.x, self.y)
end

function Viewport:lockAsTarget()
	love.graphics.push("all")
	self.lastCanvas = love.graphics.getCanvas()
	love.graphics.setCanvas(self.canvas)
end

function Viewport:releaseAsTarget()
	love.graphics.setCanvas(self.lastCanvas)
	love.graphics.pop()
end

function Viewport:useOffsetCoords()
	love.graphics.translate(-self.offsetX, -self.offsetY)
end