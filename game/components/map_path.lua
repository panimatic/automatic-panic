require 'vendor/class'
require 'components/stack'
require 'extensions/math'

MapPath = class(function(o, map)
	o.map = map
	o.nodeList = {}
	o:calculatePathGraph()
end)

function MapPath:calculatePathGraph()
	local nodeStack = Stack()

	for y = 0, self.map.mapHeight - 1, 1 do
		for x = 0, self.map.mapWidth - 1, 1 do

			local node = self:getNodeValueFromMap(x, y)
			local pathCount = 0

			local top = self:getNodeValueFromMap(x, y - 1)
			local bottom = self:getNodeValueFromMap(x, y + 1)
			local left = self:getNodeValueFromMap(x - 1, y) 
			local right = self:getNodeValueFromMap(x + 1, y)

			pathCount = top > 0 and pathCount + 1 or (bottom > 0 and pathCount + 1 or pathCount)
			pathCount = bottom > 0 and pathCount + 1 or (top > 0 and pathCount + 1 or pathCount)
			pathCount = left > 0 and pathCount + 1 or (right > 0 and pathCount + 1 or pathCount)
			pathCount = right > 0 and pathCount + 1 or (left > 0 and pathCount + 1 or pathCount)

			if 
				pathCount == 2 and
				(bottom > 0 and (top + left + right) == 0) or
				(top > 0 and (bottom + left + right) == 0) or
				(right > 0 and (top + left + bottom) == 0) or
				(left > 0 and (top + right + bottom) == 0)
			then
				pathCount = pathCount + 1
			end	

			if node > 0 and pathCount ~= 2  then
				local node = {
					x = x, 
					y = y, 
					value = node,
					neighbors = {}
				}
				table.insert(self.nodeList, node)
			end
		end
	end

	for i = 1, #self.nodeList, 1 do
		self:mapNode(self.nodeList[i])
	end
end

function MapPath:mapNode(node)
	self:addNeighbor(node, 0, 1)
	self:addNeighbor(node, 0, -1)
	self:addNeighbor(node, 1, 0)
	self:addNeighbor(node, -1, 0)
end

function MapPath:addNeighbor(node, dirX, dirY)
	local dirXCount = dirX
	local dirYCount = dirY

	local nodeX = node.x + dirXCount
	local nodeY = node.y + dirYCount

	local neighbor = self:getNode(nodeX, nodeY);

	while neighbor == nil and self:getNodeValueFromMap(nodeX, nodeY) > 0 do
		dirXCount = dirXCount + dirX
		dirYCount = dirYCount + dirY

		nodeX = node.x + dirXCount
		nodeY = node.y + dirYCount

		neighbor = self:getNode(nodeX, nodeY)
	end

	if neighbor ~= nil then
		table.insert(node.neighbors, {
			node = neighbor,
			dist = getNodeDist(node, neighbor)	
		})
	end
end

function MapPath:getNodeValueFromMap(x, y)
	if x < 0 or x > self.map.mapWidth - 1 or y < 0 or y > self.map.mapHeight - 1 then
		return 0
	end

	return self.map.mapConfig.pathways[y + 1][x + 1]
end

function MapPath:getNode(x, y)
	for index, mapNode in ipairs(self.nodeList) do
		if (mapNode.x == x and mapNode.y == y) then
			return mapNode
		end
	end

	return nil
end

function MapPath:isInPathList(x, y)
	for _, mapNode in ipairs(self.nodeList) do
		if (mapNode.x == x and mapNode.y == y) then
			return true
		end
	end

	return false
end

function MapPath:getClosestNode(mapX, mapY)
	if #self.nodeList == 0 then
		return nil
	end

	local node = self:getNode(mapX, mapY)

	if node ~= nil then
		return node
	end

	local allWallsHit = false
	local dirXCount = 1
	local dirYCount = 1
	local wallCount = 0
	local nodeCheck = {nil, nil, nil, nil}
	local mapNumCheck = {false, false, false, false}

	while allWallsHit == false do

		nodeCheck[1] = self:getNode(mapX, mapY - dirYCount)
		nodeCheck[2] = self:getNode(mapX, mapY + dirYCount)
		nodeCheck[3] = self:getNode(mapX - dirXCount, mapY)
		nodeCheck[4] = self:getNode(mapX + dirXCount, mapY)

		mapNumCheck[1] = self:getNodeValueFromMap(mapX, mapY - dirYCount) > 0
		mapNumCheck[2] = self:getNodeValueFromMap(mapX, mapY + dirYCount) > 0
		mapNumCheck[3] = self:getNodeValueFromMap(mapX - dirXCount, mapY) > 0
		mapNumCheck[4] = self:getNodeValueFromMap(mapX + dirXCount, mapY) > 0

		for i = 1, #nodeCheck, 1 do
			local posNode = nodeCheck[i]
			if posNode ~= nil then
				return posNode
			end
		end


		allWallsHit = true
		for i = 1, #mapNumCheck, 1 do


			if mapNumCheck[i] == true then
				allWallsHit = false
				break
			end
		end

		dirXCount = dirXCount + 1
		dirYCount = dirYCount + 1
	end

	return nil
end

function MapPath:getPathFromXY(startX, startY, destinationNode)
	local closestNode = self:getClosestNode(startX, startY)

	if closestNode == nil then
		return {}
	end

	return self:getPathFromNode(closestNode, destinationNode)
end


local function notIn(nodeSet, checkNode)
	for _, node in ipairs (nodeSet) do
		if node == checkNode then
			return false
		end
	end
	return true
end

local function getLowestNode(nodeSet, fScoreNodes)
	local lowestNode = nodeSet[1]
	local lowestScore = fScoreNodes[lowestNode]

	for _, node in ipairs (nodeSet) do
		local score = fScoreNodes[node]

		if score < lowestScore then
			lowestScore = score
			lowestNode = node
		end
	end

	return lowestNode
end

local function unwindPath(flatPath, map, currentNode)

	if map[currentNode] then
		table.insert(flatPath, 1, map[currentNode]) 
		return unwindPath(flatPath, map, map[currentNode])
	else
		return flatPath
	end
end


local function switchNode(set, checkNode)
	for i, node in ipairs(set) do
		if node == checkNode then 
			set[i] = set[#set]
			set[#set] = nil
			break
		end
	end	
end


function getNodeDist(node1, node2)
	return math.dist(node1.x, node1.y, node2.x, node2.y)
end

function MapPath:getPathFromNode(startNode, endNode)

	local closedSet = {}
	local openSet = {startNode}
	local cameFrom = {}
	local gScoreNodes = {}
	local fScoreNodes = {}

	gScoreNodes[startNode] = 0
	fScoreNodes[startNode] = gScoreNodes[startNode] + getNodeDist(startNode, endNode)


	while #openSet > 0 do

		local currentNode = getLowestNode(openSet, fScoreNodes)

		if currentNode == endNode then
			local path = unwindPath({}, cameFrom, endNode)
			table.insert(path, endNode)
			return path
		end

		switchNode(openSet, currentNode)
		table.insert(closedSet, currentNode)

		for _, neighborInfo in ipairs (currentNode.neighbors) do 
			local neighbor = neighborInfo.node

			if notIn(closedSet, neighbor) then
				local checkGScore = gScoreNodes[currentNode] + neighborInfo.dist
				 
				if notIn(openSet, neighbor) or checkGScore < gScoreNodes[neighbor] then 
					cameFrom[neighbor] = currentNode
					gScoreNodes[neighbor] = checkGScore
					fScoreNodes[neighbor] = gScoreNodes[neighbor] + getNodeDist(neighbor, endNode)

					if notIn(openSet, neighbor) then
						table.insert(openSet, neighbor)
					end

				end
			end
		end

	end

	return nil
end