require 'vendor/class'

Polygon = class(function(o, data)
	o.points = data
	o.edges = {}
end
)

function Polygon:buildEdges() 
	local p1 = Vector.new()
	local p2 = Vector.new()
	self.edges = {}
	for i = 1, #self.points do
		p1 = self.points[i];
		if i + 1 > #self.points then
			p2 = self.points[1];
		else
			p2 = self.points[i + 1];
		end
		table.insert(self.edges, (p2 - p1))
	end
end

function Polygon:draw() 
	love.graphics.setColor(255, 240, 240, 255)
	for i = 1, #self.points do	
		p1 = self.points[i];
		if i + 1 > #self.points then
			p2 = self.points[1];
		else
			p2 = self.points[i + 1];
		end
		love.graphics.line(p1.x, p1.y, p2.x, p2.y)
	end
end

