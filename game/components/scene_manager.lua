require '../vendor/class'

SceneManager = class(function(o, scene)
	o.scene = nil
	o.storedScenes = {}
	o:setScene(scene)
end)

function SceneManager:setScene(scene)
	self.scene = scene
end

function SceneManager:storeCurrentScene(sceneKey)
	if not self.scene then return end
	self.storedScenes[sceneKey] = self.scene
end

function SceneManager:restoreScene(sceneKey)
	if self.storedScenes[sceneKey] ~= nil then return end
	self.scene = self.storedScenes[sceneKey]
end

function SceneManager:draw()
	if not self.scene then return end
	self.scene:draw()
end

function SceneManager:update(dt)
	if not self.scene then return end
	self.scene:update(dt)
end


function SceneManager:load()
	if not self.scene then return end
	self.scene:load()
end

function SceneManager:mousepressed(x, y, button)
	if not self.scene then return end
	self.scene:mousepressed(x, y, button)
end

function SceneManager:mousereleased(x, y, button)
	if not self.scene then return end
	self.scene:mousereleased(x, y, button)
end

function SceneManager:keypressed(key, scancode, isrepeat)
	if not self.scene then return end
	self.scene:keypressed(key, scancode, isrepeat)
end

function SceneManager:mousemoved(x, y, dx, dy)
	if not self.scene then return end
	self.scene:mousemoved(x, y, dx, dy)
end