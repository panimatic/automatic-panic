require 'vendor/class'

Stack = class(function(o)
	o.lastIndex = -1
	o.items = {}
end)

function Stack:push(item)
	table.insert(self.items, item)
	self.lastIndex = self.lastIndex + 1
end

function Stack:pop(item)
	if self:isEmpty() then
		return nil
	end

	local poppedItem = self.items[self.lastIndex]

	table.remove(self.items, self.lastIndex)
	self.lastIndex = self.lastIndex - 1

	return poppedItem
end

function Stack:isEmpty()
	return self.lastIndex == -1
end