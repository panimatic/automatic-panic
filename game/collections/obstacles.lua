require 'vendor/class'
require 'collections/object_group'
require 'objects/obstacle'

local json = require 'vendor/json'

Obstacles = class(ObjectGroup, function(o, data)
	ObjectGroup.init(o)
	local mapFileData, size = love.filesystem.read("maps/map.json")

	local mapConfig = json.decode(mapFileData)
	for _, item in ipairs(mapConfig.obstacles) do
		local obstacle = Obstacle({
				x = item.x,
				y = item.y,
				height = item.height,
				width = item.width,
				color = {140,255,216,200},
				isBeingPressed = false
			})
		table.insert(o.objects, obstacle)
	end
	
end)