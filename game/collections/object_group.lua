require 'objects/moveable_object'
require 'objects/active_object'

ObjectGroup = class(function(o)
	o.objects = {}
	o.offsetX = 0
	o.offsetY = 0
	o.viewport = nil
end)

function ObjectGroup:getCount()
	return #self.objects
end

function ObjectGroup:add(object)
	if self:hasObject(object) then
		return
	end

	object:setViewport(self.viewport)

	table.insert(self.objects, object)
end

function ObjectGroup:clear()
	self.objects = {}
end

function ObjectGroup:hasObject(object)
	for _, itemInside in ipairs(self.objects) do
		if object == itemInside then
			return true
		end
	end

	return false
end

function ObjectGroup:removeByIndex(itemIndex)
	for index, item in ipairs(self.objects) do
		if itemIndex == index then
			table.remove(self.objects, index)
			break
		end
	end
end

function ObjectGroup:remove(object)
	for index, currentItem in ipairs(self.objects) do
		if object == currentItem then
			table.remove(self.objects, index)
			break
		end
	end
end

function ObjectGroup:isOffScreen()
	if self.viewport == nil then
		return false
	end

	for _, object in ipairs(self.objects) do
		if not object:isOffScreen() then
			return false
		end
	end

	return true
end

function ObjectGroup:setViewport(viewport)
	self.viewport = viewport

	for _, object in ipairs(self.objects) do
		object:setViewport(viewport)
	end
end

function ObjectGroup:press(x, y, button)
	for _, object in ipairs(self.objects) do
		if object.press ~= nil then
			object:press(x + self.offsetX,y  + self.offsetY, button)
		end
	end
end

function ObjectGroup:release(button)
	for _, object in ipairs(self.objects) do
		if object.release ~= nil then
			object:release(button)
		end
	end
end

function ObjectGroup:move(x, y)
	for _, object in ipairs(self.objects) do
		if object.move ~= nil and object:move(x + self.offsetX, y + self.offsetY) then	
			return true
		end
	end
	return false
end

function ObjectGroup:draw()
	for _, object in ipairs(self.objects) do
		object:draw()
	end
end

function ObjectGroup:update(dt)
	for _, object in ipairs(self.objects) do
		object:update(dt)
	end
end

function ObjectGroup:updateMousePosition(x, y)
	for _, object in ipairs(self.objects) do
		if object.updateMousePosition ~= nil then
			object:updateMousePosition(x + self.offsetX,y  + self.offsetY)
		end
	end
end

function ObjectGroup:getByPoint(x, y)
	for _, object in ipairs(self.objects) do
		if (object:isPointInside(x,y)) then
			return object
		end
	end

	return nil
end

function ObjectGroup:setOffset(x, y)
	self.offsetX = x
	self.offsetY = y
end