require 'collections/object_group'

TowerGenerators = class(ObjectGroup, function(o, towerList)
	o.towerList = towerList
	ObjectGroup.init(o)
end)

function TowerGenerators:getGenerator(x, y, button)
	if button == 1 then
		return self:getByPoint(x, y)
	end
	return nil
end


function TowerGenerators:press(x, y, button)
	ObjectGroup.press(self, x, y, button)
	
	local generator = self:getGenerator(x, y, button)

	if not generator then
		return
	end

	local tower = generator:getTower(x + self.offsetX, y + self.offsetY)

	self.towerList:add(tower)
end

function TowerGenerators:setTowersObserver(manager)
	for i, object in ipairs(self.objects) do
		self.objects[i]:setTowersObserver(manager)
	end
end