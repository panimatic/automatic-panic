require 'vendor/class'
require 'collections/object_group'

Towers = class(ObjectGroup, function(o, data)
	ObjectGroup.init(o)
end)

function Towers:press(x, y, button)
	local towersCount = self:getCount()

	for i, object in ipairs(self.objects) do
		local index = towersCount + 1 - i	
		if object:press(x, y, button) then
			table.insert(self.objects, object)
			table.remove(self.objects, index)
			return
		end
	end
end

function Towers:setTowersObserver(manager)
	for i, object in ipairs(self.objects) do
		self.objects[i]:setTowersObserver(manager)
	end
end
