require 'vendor/class'
require 'objects/active_object'
require 'extensions/math'

Enemy = class(ActiveObject, function(o, data) 
	o.range = 0
	o.mapX = 0
	o.mapY = 0
	o.health = 100
	o.isDead = false
	o.damageFlashTime = 0.18
	o.passedFlashTime = 0
	o.wasShot = false
	o.tileSize = 0
	o.moveDiffX = 0
	o.moveDiffY = 0
	o.moveStartX = 0
	o.moveStartY = 0
	o.rotationStart = 0
	o.rotationDiff = 0
	o.moveSpeed = 50
	o.rotationSpeed = 10
	o.pathDiffList = nil
	o.minDiffDist = 0
	o.maxDiffDist = 0
	o.moveTime = 0
	o.isMoveAllowed = false
	o.destinationNode = nil
	o.timeDirection = 1
	o.collisionRadius = math.outerRectRadius(data.width, data.height)
 	MovableObject.init(o, data)
end)

function Enemy:setPath(pathNodeList)
	if pathNodeList == nil or #pathNodeList == 0 then
		self.isMoveAllowed = false
		self.pathDiffList = nil
		return
	end

	self.isMoveAllowed = true
	self.moveTime = 0
	self.pathDiffList = {}

	local oldNode = nil

	for _, node in ipairs(pathNodeList) do
		if oldNode ~= nil then
			local nX = node.x * self.tileSize 
			local nY = node.y * self.tileSize 			
			local oX = oldNode.x * self.tileSize 
			local oY = oldNode.y * self.tileSize 
			table.insert(self.pathDiffList, {
				dx = nX - oX, 
				dy = nY - oY,
				x = nX,
				y = nY,
				dist = math.dist(nX, nY, oX, oY)
			})
		end

		oldNode = node
	end

	self.minDiffDist = 0
	self.maxDiffDist = self.pathDiffList[1].dist

	self.moveDiffX = self.pathDiffList[1].dx
	self.moveDiffY = self.pathDiffList[1].dy

	self.moveStartX = self.x
	self.moveStartY = self.y
	local newangle = math.angle(0, 0, self.moveDiffX, self.moveDiffY)
	self.rotationStart = newangle
	self.rotation = newangle
	print(self.moveDiffX, self.moveDiffY)
	print("start", self.rotationStart * 180 / math.pi)
end

function Enemy:draw()
	if self:isOffScreen() then
		return
	end

	love.graphics.push()
	self:applyTransformations()

	if self.isMouseOver then
		love.graphics.setColor(216, 237, 21, 255)
	else
		love.graphics.setColor(self.color[1], self.color[2], self.color[3], self.color[4])
	end

	love.graphics.polygon('fill', self.x, self.y, self.x + self.width, self.y + self.height/2, self.x, self.y + self.height)

	love.graphics.setColor(50, 127, 100, 255)
	love.graphics.rectangle('fill', self.x + self.width -10, self.y + self.width / 2 - 5, 10, 10)

	love.graphics.pop()

	Object.drawCount = Object.drawCount + 1
end

function Enemy:isPointInRange(x,y)
	if self:getDistance(x,y) <= self.range then
	     return true
    end 
	
	return false
end

function Enemy:getDistance(x,y)
	return math.dist(self:getCenterX(), self:getCenterY(), x, y)
end

function Enemy:update(dt)
	if self.isDead then
	    return
	end
	self.dt = dt
	if self.wasShot then
	    self.passedFlashTime = self.passedFlashTime + self.dt
	end
	
	if self.passedFlashTime >= self.damageFlashTime then
	    self.color = {140,255,216,200}
	    self.passedFlashTime = 0
	    self.wasShot = false
	end
	
	self.moveTime = self.moveTime + dt * self.moveSpeed * self.timeDirection

	if self.isMoveAllowed then

		if self.moveTime < 0 then
			self.moveTime = 0
		end

		if (self.timeDirection == 1 and self.moveTime > self.maxDiffDist) or (self.timeDirection == -1 and self.moveTime < self.minDiffDist) then
			if not self:setNewDiff() then
				if self.timeDirection == 1 or (self.timeDirection == -1 and self.moveTime == 0) then
					self.isMoveAllowed = false
				end
			end
			self.x = self.moveStartX + self.moveDiffX
			self.y = self.moveStartY + self.moveDiffY
		end

		if self.moveTime <= self.maxDiffDist then
			self:rotateToward(self.x + self.moveDiffX, self.y + self.moveDiffY)
			local movePercentage = (self.moveTime - self.minDiffDist) / (self.maxDiffDist - self.minDiffDist)

			self.x = self.moveStartX + self.moveDiffX * movePercentage
			self.y = self.moveStartY + self.moveDiffY * movePercentage
		end

		self:updateVisibleRect()
	end
end

function Enemy:setNewDiff()
	if self.pathDiffList == nil then
		return
	end

	local totalDist = 0
	local newNode = nil

	for index, node in ipairs(self.pathDiffList) do
		if totalDist + node.dist > self.moveTime then
			newNode = node
			break;
		end

		totalDist = totalDist + node.dist
	end

	if newNode == nil then
		return false
	end

	self.moveDiffX = newNode.dx
	self.moveDiffY = newNode.dy

	self.minDiffDist = totalDist
	self.maxDiffDist = totalDist + newNode.dist
	self.moveStartX = newNode.x - newNode.dx
	self.moveStartY = newNode.y - newNode.dy
	
	local newangle = math.angle(0, 0, self.moveDiffX, self.moveDiffY)
	self.rotationDiff = newangle
	return true
end

function Enemy:rotateToward(x, y)
	local angleDifference = self.rotation - self.rotationDiff
	local absAngleDifference = math.abs(angleDifference)


	if absAngleDifference > math.pi then
	    angleDifference = - math.sign(angleDifference)  * (absAngleDifference)
	end

	if absAngleDifference < 0.03 then
	    rot =self.rotationDiff
	else
	    rot = self.rotation - math.sign(angleDifference) * self.dt * self.rotationSpeed
	end

	self:setRotation(rot)
	
end

function Enemy:damage(amount)
	self.color = {255, 0, 0, 255}
	self.health = self.health - amount
	self.wasShot = true
	if self.health <= 0 then
		self.isDead = true
	    self.color = {255, 0, 216, 200}
	end
end