require 'vendor/class'
require 'objects/active_object'

MovableObject = class(ActiveObject, function(o, data) 
	o.dt = 1
	o.obstacles = {}
	o.canBeDropped = true
 	ActiveObject.init(o, data)
  	o.mouseX, o.mouseY = love.mouse.getPosition()
end)

function MovableObject:mousepressed(x, y, button)
end

function MovableObject:mousereleased(x, y, button)
end

function MovableObject:mousemoved(x, y, dx, dy)
end

function MovableObject:move(x, y)	
	if self.isBeingPressed then
		self:setCenteredPosition(self.mouseX, self.mouseY)
		self:updatePoints()
		return true
	end
	return false
end

function MovableObject:updateMousePosition(x, y)
	self.mouseX = x
	self.mouseY = y 

	ActiveObject.updateMousePosition(self, x, y)
end

function MovableObject:updatePoints()
end

function MovableObject:press(x, y, button)
	if button == 1 and self.isMouseOver then
		if self.isBeingPressed and self.canBeDropped then
			self.isBeingPressed = false
		elseif self.isBeingPressed == false then
		    self.isBeingPressed = true
		end
	end
end