require 'vendor/class'
require 'objects/object'

Window = class(Object, function(self, x, y, width, height)
	self.x = x ~= nil and x or 0
	self.y = y ~= nil and y or 0
	self.width = width ~= nil and width or 200
	self.height = height ~= nil and height or 200
	self.borderWidth = 3
	self.windowColor = {34, 119, 156, 255}
	self.drawBorder = true
	self.opacity = 0
	self.isVisible = 0

	self._lastCanvas = nil
	self._fadeTo = false
	self._fadeToSpeed = 200
	self._fadeToOpacity = 0
	self._fadeDirection = 0
	self._canvasRect = {
		x = 3,
		y = 3,
		width = self.width - 3,
		height = self.height - 3
	}

	self._canvas = love.graphics.newCanvas(self._canvasRect.width, self._canvasRect.height)
end)

function Window:setRect(x, y, width, height)
	self:setPosition(x, y)
	self:setSize(width, height)
end

function Window:setPosition(x, y)
	self.x = x
	self.y = y
end

function Window:setSize(width, height)
	self.width = width
	self.height = height
	self:_checkCanvasSize(width, height)
end

function Window:_checkCanvasSize(width, height)
	local canvasPosWidth = self._canvasRect.width + self._canvasRect.x
	local canvasPosHeight = self._canvasRect.height + self._canvasRect.y

	local heightDiff = math.max(canvasPosHeight - height, 0)
	local widthDiff = math.max(canvasPosWidth - width, 0)

	if (widthDiff == 0) and (heightDiff == 0) then
		return
	end

	self._canvasRect.width = self._canvasRect.width - widthDiff
	self._canvasRect.height = self._canvasRect.height - heightDiff

	local newCanvas = love.graphics.newCanvas(self._canvasRect.width, self._canvasRect.height)
	local lastCanvas = love.graphics.getCanvas()

	love.graphics.push("all")
	love.graphics.setCanvas(newCanvas)

	love.graphics.setBlendMode("replace")
	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.draw(self._canvas)

	love.graphics.setCanvas(lastCanvas)
	love.graphics.pop()

	self._canvas = newCanvas
end

function Window:setCanvasRect(x, y, width, height)
	self._canvasRect.x = x
	self._canvasRect.y = y
	self._canvasRect.width = width
	self._canvasRect.height = height

	self._checkCanvasSize(self.width, self.height)
end

function Window:lockAsTarget()
	love.graphics.push("all")
	self._lastCanvas = love.graphics.getCanvas()
	love.graphics.setCanvas(self._canvas)
end

function Window:unlockAsTarget()
	love.graphics.setCanvas(self._lastCanvas)
	love.graphics.pop()
end

function Window:fadeTo(opacity, speed)
	self._fadeTo = true
	if speed ~= nil then
		self._fadeToSpeed = speed
	end
	self._fadeToOpacity = opacity
	self._fadeDirection = opacity > self.opacity and 1 or -1
end

function Window:draw()
	if self.opacity == 0 or not self.isVisible then
		return
	end

	love.graphics.push("all")

	love.graphics.setBlendMode("alpha")
	love.graphics.setColor(self.windowColor[1], self.windowColor[2], self.windowColor[3], self.windowColor[4] * self.opacity / 255)
	love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)

	if self.drawBorder then
		love.graphics.setLineWidth(self.borderWidth)
		love.graphics.rectangle("line", self.x, self.y, self.width, self.height)
	end

	love.graphics.setColor(255, 255, 255, self.opacity)
	love.graphics.draw(self._canvas, self.x, self.y)

	love.graphics.pop()
end

function Window:update(dt)
	if self._fadeTo then
		self.opacity = self.opacity + self._fadeToSpeed * self._fadeDirection * dt
		if 
			(self._fadeDirection == 1 and self.opacity > self._fadeToOpacity) or 
			(self._fadeDirection == -1 and self.opacity < self._fadeToOpacity)
		then
			self.opacity = self._fadeToOpacity
			self._fadeTo = false
		end
	end
end