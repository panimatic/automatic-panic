require 'vendor/class'
require 'collections/object_group'
require 'components/viewport'
require 'extensions/math'

local json = require 'vendor/json'

Map = class(Object, function(o, fileName, width, height)
	o.pathways = {}

	o.viewport = Viewport(0, 0, width, height)

	o.tileSize = 32
	o.offScreenTileCount = 4
	o.wTileCount = math.ceil(o.viewport.width / o.tileSize) + o.offScreenTileCount
	o.hTileCount = math.ceil(o.viewport.height / o.tileSize) + o.offScreenTileCount

	o.tilemap = love.graphics.newCanvas(o.wTileCount * o.tileSize, o.hTileCount * o.tileSize) 

	local mapFileData, size = love.filesystem.read(fileName)

	o.mapConfig = json.decode(mapFileData)
	o.mapHeight = #o.mapConfig.pathways
	o.mapWidth = #o.mapConfig.pathways[1]

	o.pixelMapWidth = o.mapWidth * o.tileSize
	o.pixelMapHeight = o.mapHeight * o.tileSize

	o.offsetX = 0
	o.offsetY = 0

	o.deltaOffsetX = 0
	o.deltaOffsetY = 0

	o.mpStartX = 0
	o.mpEndX = 0
	o.mpStartY = 0
	o.mpEndY = 0

	o.objectGroup = ObjectGroup()
	o.objectGroup:setViewport(o.viewport)

	o.mapPath = MapPath(o)
	o.endNode = nil

	for _, node in ipairs(o.mapPath.nodeList) do
		if node.value == 2 then
			o.endNode = node
			break
		end
	end

	o:updateViewport(0, 0)
end)

function Map:updateViewport(ox, oy)

	ox = math.clamp(0, ox, self.pixelMapWidth - self.viewport.width)
	oy = math.clamp(0, oy, self.pixelMapHeight - self.viewport.height)

	local tileX = math.max(math.ceil(ox / self.tileSize) - self.offScreenTileCount, 0)
	local tileY = math.max(math.ceil(oy / self.tileSize) - self.offScreenTileCount, 0)

	self:renderMap(tileX, tileY)

	self.offsetX = ox - self.mpStartX * self.tileSize;
	self.offsetY = oy - self.mpStartY * self.tileSize

	self.viewport:setOffset(ox, oy)

	self.viewport:lockAsTarget()

	love.graphics.clear()
	love.graphics.setColor(255, 0, 0, 255)
	love.graphics.draw(self.tilemap, -self.offsetX, -self.offsetY)

	self.viewport:useOffsetCoords()
	
	self:renderPaths()


	self.objectGroup:setOffset(ox, oy)

	Object.drawCount = 0
	self.objectGroup:draw()

	self.viewport:releaseAsTarget()
end

function Map:draw()
	love.graphics.push("all")
	love.graphics.setColor(255, 255, 255, 255)
	self.viewport:draw()

	love.graphics.print(
		"mpSX: " .. tostring(self.mpStartX  * self.tileSize) .. 
		", mpSY: " .. tostring(self.mpStartY * self.tileSize) .. 		
		", mpEX: " .. tostring(self.mpEndX  * self.tileSize) .. 
		", mpEY: " .. tostring(self.mpEndY * self.tileSize) .. 
		", oX:" .. tostring(self.offsetX) .. 
		", oY: " .. tostring(self.offsetY) ..
		", Objects: " .. tostring(Object.drawCount) 
		, 
	10, 30)

	love.graphics.pop()
end

function Map:renderPaths()
	love.graphics.setColor(255, 255, 255, 255)
	for i = 1, #self.mapPath.nodeList, 1 do
	
		local node = self.mapPath.nodeList[i]
		local os = self.tileSize / 2
		local nodeX = node.x * self.tileSize + os
		local nodeY = node.y * self.tileSize + os

		for j = 1, #node.neighbors, 1 do
			local neighbor = node.neighbors[j].node

			love.graphics.line(nodeX, nodeY, neighbor.x * self.tileSize + os, neighbor.y * self.tileSize + os)
		end

	end


	
	for i = 1, #self.mapPath.nodeList, 1 do

		local node = self.mapPath.nodeList[i]

		local nodeX = node.x * self.tileSize + self.tileSize / 2 
		local nodeY = node.y * self.tileSize + self.tileSize / 2

		if node.value ~= 2 then
			love.graphics.setColor(255, 127, 0, 255)
		else
			love.graphics.setColor(127, 127, 255, 255)
		end

		love.graphics.circle('fill', nodeX, nodeY, 20, 80)
	end
end

function Map:renderMap(ox, oy)
	local sX = math.clamp(0, ox, self.mapWidth - self.wTileCount)
	local sY = math.clamp(0, oy, self.mapHeight - self.hTileCount)
	
	local eX = math.min(sX + self.wTileCount, self.mapWidth - 1)
	local eY = math.min(sY + self.hTileCount, self.mapHeight - 1)

	if (
			sX == self.mpStartX and
			sY == self.mpStartY and
			eX == self.mpEndX and
			eY == self.mpEndY
		) then
		return
	end

	love.graphics.setCanvas(self.tilemap)
	love.graphics.clear()
	love.graphics.setColor(255, 0, 0, 255)

	for y = sY, eY, 1 do
		local pathLine = y < self.mapHeight and self.mapConfig.pathways[y + 1] or {}

		for x = sX, eX, 1 do
			local drawMode = pathLine[x + 1] == 1 and 'fill' or 'line'

			love.graphics.rectangle(drawMode, 
				self.tileSize * (x - sX), 
				self.tileSize * (y - sY), 
				self.tileSize, 
				self.tileSize
			)
		end
	end
    
	love.graphics.setCanvas()

	self.mpStartX = sX
	self.mpStartY = sY
	self.mpEndX = eX
	self.mpEndY = eY
end

function Map:update(dt)
	local x, y = love.mouse.getPosition()

	self.objectGroup:updateMousePosition(x, y)
	self.objectGroup:update(dt)
end

function Map:addObject(object)
	self.objectGroup:add(object)
end

function Map:removeObject(object)
	self.objectGroup:remove(object)
end

function Map:processMouseMove(x, y, dx, dy)
	self.objectGroup:move(x, y)
end

function Map:processMouseReleased(x, y, button)
	self.objectGroup:release(button)
end

function Map:processMousePressed(x, y, button)
	self.objectGroup:press(x, y, button)
end