require '../vendor/class'
require '/objects/active_object'

Obstacle = class(ActiveObject, function(o,data) 
 	ActiveObject.init(o, data)
 	o.polygon = Polygon({
		Vector.new(o.x, o.y),
		Vector.new(o.x, o.y + o.height),
		Vector.new(o.x + o.width, o.y + o.height),
		Vector.new(o.x + o.width, o.y),
	})
	o.polygon:buildEdges() 

end)

function Obstacle:draw()
	if self:isOffScreen() then
		return
	end

	love.graphics.push()
	self:applyTransformations()

	love.graphics.setColor(self.color[1], self.color[2], self.color[3], self.color[4])

	self.polygon:draw()

	love.graphics.pop()

	Object.drawCount = Object.drawCount + 1
end