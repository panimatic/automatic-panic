require 'vendor/class'
require 'extensions/math'
require 'components/collision_detector'

Object = class(function(o, data)
	o.x = 0
	o.y = 0
	o.rX = 0
	o.rY = 0
	o.width = 0
	o.height = 0
	o.diagonal = 0
	o.outerCircleRadius = 0
	o.rotation = 0
	o.color = {0,0,0,0}
	o.offsetX = 0
	o.offsetY = 0
	o.viewport = nil
	o.outerRectX = 0
	o.outerRectY = 0

	for k, v in next, data do
      o[k] = v
    end

    o:setPosition(o.x, o.y)
    o:setSize(o.width, o.height)
end
)

Object.drawCount = 0

function Object:setOffset(x, y)
	self.offsetX = x
	self.offsetY = y
end

function Object:isOffScreen()
	if self.viewport == nil then
		return false
	end

	return not self.viewport:isInside(self.outerRectX + self.offsetX, self.outerRectY + self.offsetY, self.diagonal, self.diagonal)
end

function Object:setViewport(viewport)
	self.viewport = viewport
end

function Object:draw()
end

function Object:setPosition(x, y)
	self.x = x
	self.y = y
	self.rX = x + self.width
	self.rY = x + self.height
end

function Object:setSize(width, height)
	self.width = width
	self.height = height
	self:updateVisibleRect()
end

function Object:updateVisibleRect()
	self.diagonal = (self.width ^ 2 + self.height ^ 2) ^ 0.5
	self.outerCircleRadius = self.diagonal / 2
	self.outerRectX = self:getCenterX() - self.outerCircleRadius
	self.outerRectY = self:getCenterY() - self.outerCircleRadius
end

function Object:setColor(r, g, b, a)
	self.color = {r, g, b, a}
end

function Object:setCenteredPosition(x, y)
	self.x = x - self.width / 2
	self.y = y - self.height / 2

	self:updateVisibleRect()
end

function Object:update()
end

function Object:getCenterX()
	return self.x + self.width / 2
end

function Object:getCenterY()
	return self.y + self.height / 2
end

function Object:applyTransformations()
	love.graphics.translate(self:getCenterX(), self:getCenterY())
  	love.graphics.rotate(self.rotation)
  	love.graphics.translate(-self:getCenterX(), -self:getCenterY())
end

function Object:setRotation(rotation)
	if rotation == nil then
	    rotation = 0
	end
	local absRotation = math.abs(rotation)
	if absRotation > (math.pi) then
	    rotation = -math.sign(rotation) * (2*math.pi - absRotation) 
	end
	self.rotation = rotation
end




