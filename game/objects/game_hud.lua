require 'vendor/class'
require 'components/stack'
require 'extensions/math'
require 'objects/window'

GameHUD = class(Object, function(self, map)
	self.map = map
	self.generators = {}

	self._screenWidth, self._screenHeight = love.graphics.getDimensions()

	self._descWindow = Window(0, self._screenHeight - 200, self._screenWidth - 300, 200)
	self._descWindow:fadeTo(80)

	self._gameWindow = Window(self._screenWidth - 300, self._screenHeight - 200, 300, 200)
	self._gameWindow:fadeTo(80)
	self:renderGameWindowData()
end)

function GameHUD:renderGameWindowData()
	self._descWindow:lockAsTarget()

	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.rectangle("line", 15, 25, 150, 150)

	self._descWindow:unlockAsTarget()
end

function GameHUD:draw()
	self._descWindow:draw()
	self._gameWindow:draw()
end

function GameHUD:update(dt)
	self._descWindow:update(dt)
	self._gameWindow:update(dt)
end