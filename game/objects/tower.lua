require '../vendor/class'
require '/objects/moveable_object'
require 'components/polygon'
require 'extensions/math'

Tower = class(MovableObject, function(o, data) 
	o.range = 0
	o.rotationSpeed = 4
	o.isLockedOnEnemy = false
	o.shootingSpeed = 5
	o.damage = 20
	o.timeSinceLastShoot = 0
 	MovableObject.init(o, data)
 	o.polygon = Polygon({
		Vector.new(o.x, o.y),
		Vector.new(o.x, o.y + o.height),
		Vector.new(o.x + o.width, o.y + o.height),
		Vector.new(o.x + o.width, o.y),
	})
	o.enemy = nil
	o.polygon:buildEdges() 
end)

function Tower:draw()
	local centerX = self:getCenterX()
	local centerY = self:getCenterY()
	if self:isOffScreen() then
		return
	end
	self.polygon:draw()

	love.graphics.push()
	self:applyTransformations()

	love.graphics.setColor(self.color[1], self.color[2], self.color[3], self.color[4])

	if self.isMouseOver then
		love.graphics.setColor(216, 237, 21, 255)
	else
		love.graphics.setColor(self.color[1], self.color[2], self.color[3], self.color[4])
	end

	if not self.canBeDropped then
		love.graphics.setColor(self.color[1], self.color[2], self.color[3], 100)
	end

	
	local r = self.outerCircleRadius
	local rectW = self.diagonal / 2

	love.graphics.rectangle('fill', centerX - self.physicalWidth/2, centerY - self.physicalHeight/2, self.physicalWidth, self.physicalHeight)
	love.graphics.setColor(0, 0, 0, 255)
	love.graphics.rectangle('fill', self:getCenterX()+5, self:getCenterY()-2, 4, 4)
	love.graphics.setColor(self.color[1], self.color[2], self.color[3], self.color[4])
	love.graphics.circle( 'line', self:getCenterX(), self:getCenterY(), r * 2 - 2^.5 * r, 80)
	love.graphics.pop()

	Object.drawCount = Object.drawCount + 1
end

function Tower:isPointInRange(x,y)
	if self:getDistance(x,y) <= self.range then
	     return true
	 end 
	 return false
end

function Tower:getDistance(x,y)
	return math.dist(self:getCenterX(), self:getCenterY(), x, y)
end

function Tower:update(dt)
	self.dt = dt

	if CollisionDetector:areObjectsColiding(self, self.obstacles) then
	    self.canBeDropped = false
	else
	    self.canBeDropped = true
	end
	if not self.isLockedOnEnemy then
	    self.color = {140, 255, 216, 200}
	end
	self:aim()
end

function Tower:aim()
	if self.enemy == nil then
		self.towersObserver:update(self)
	    return
	end
	if not self:isEnemyInRange() or self.enemy.isDead then
		self.isLockedOnEnemy = false
		self.enemy = nil
	    return
	end
	
	local angleDifference = self.rotation - math.angle(self:getCenterX(),self:getCenterY(), self.enemy.x,self.enemy.y)
	local absAngleDifference = math.abs(angleDifference)

	if absAngleDifference < 0.05 then
	    self.isLockedOnEnemy = true
	else
	    self.isLockedOnEnemy = false
	end
	self:shoot()

	if absAngleDifference > math.pi then
	    angleDifference = - math.sign(angleDifference)  * (absAngleDifference - math.pi)
	end
	if absAngleDifference < 0.01 then
	    rot = math.angle(self:getCenterX(),self:getCenterY(), self.enemy.x,self.enemy.y)
	else
	    rot = self.rotation - math.sign(angleDifference) * self.dt * self.rotationSpeed
	end
	self:setRotation(rot)
	self:updatePoints()
end

function Tower:updatePoints()
	local centerX = self:getCenterX()
	local centerY = self:getCenterY()
	local center = {x = centerX,  y = centerY}
	local point1 = math.rotatePoint({x= centerX - self.physicalWidth/2 , y = centerY - self.physicalHeight/2}, center, self.rotation)
	local point2 = math.rotatePoint({x= centerX - self.physicalWidth/2 , y = centerY + self.physicalHeight/2}, center, self.rotation)
	local point3 = math.rotatePoint({x= centerX + self.physicalWidth/2 , y = centerY + self.physicalHeight/2}, center, self.rotation)
	local point4 = math.rotatePoint({x= centerX + self.physicalWidth/2 , y = centerY - self.physicalHeight/2}, center, self.rotation)
	
	self.polygon = Polygon({
		Vector.new(point1.x, point1.y),
		Vector.new(point2.x, point2.y),
		Vector.new(point3.x, point3.y),
		Vector.new(point4.x, point4.y),
	})
	self.polygon:buildEdges() 
end

function Tower:setTowersObserver(manager)
	self.towersObserver = manager
end

function Tower:setEnemy(enemy)
	self.enemy = enemy
	
end

function Tower:isObjectInRange(object)
	return math.dist(self:getCenterX(),self:getCenterY(), object.x, object.y) < self.range + object.collisionRadius
end

function Tower:isEnemyInRange()
	return self:isObjectInRange(self.enemy)
end

function Tower:shoot()
	self.timeSinceLastShoot = self.timeSinceLastShoot + self.dt
	if self.isLockedOnEnemy and self.timeSinceLastShoot >= 1/self.shootingSpeed then
		self.color = {255,255,255, 255}
	    self.enemy:damage(self.damage)
	    self.timeSinceLastShoot = 0
	    return
	end
	if self.timeSinceLastShoot >= 0.05 then
	    self.color = {140, 255, 216, 200}
	end
	
end







