require 'vendor/class'
require 'objects/object'

ActiveObject = class(Object, function(o, data) 
	o.isBeingPressed = false
	o.isMouseOver = false
	o.polygon = null
	o.points = null
	o.physicalWidth = null
	o.physicalHeight = null
 	Object.init(o, data)
end)

function ActiveObject:isPointInside(x,y)
	return CollisionDetector:isPointInObject(self, x, y)
end

function ActiveObject:press(x, y, button)
	if button == 1 and self.isMouseOver then
		self.isBeingPressed = not self.isBeingPressed
	end
end

function ActiveObject:updateMousePosition(x, y)
	if self:isPointInside(x, y) then
		self.isMouseOver = true
		return
	end

	self.isMouseOver = false
end

function ActiveObject:release(button)
end