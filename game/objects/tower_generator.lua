require '../vendor/class'
require '/objects/active_object'

TowerGenerator = class(ActiveObject, function(o,data) 
	o.collisionManager = nil
 	ActiveObject.init(o, data)
end)

function TowerGenerator:draw()
	if self:isOffScreen() then
		return
	end

	if self.isMouseOver then
		love.graphics.setColor(216, 237, 21, 255)
	else
		love.graphics.setColor(self.color[1], self.color[2], self.color[3], self.color[4])
	end

	love.graphics.rectangle('fill', self.x, self.y, self.width, self.height)

	Object.drawCount = Object.drawCount + 1
end

function TowerGenerator:getTower(x,y)
	local tower = Tower({
			height = 160,
			width = 160,
			physicalWidth = 40,
			physicalHeight = 40,
			color = {140,255,216,200},
			range = 80,
			isBeingPressed = true,
			obstacles = self.obstacles
		})
	
	tower:setCenteredPosition(x, y)
	tower:setTowersObserver(self.collisionManager)
	return tower
end

function TowerGenerator:setTowersObserver(manager)
	self.collisionManager = manager
end


