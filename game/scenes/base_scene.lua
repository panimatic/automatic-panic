require '../vendor/class'

BaseScene = class()

function BaseScene:draw()
end

function BaseScene:update(dt)
end

function BaseScene:load()
end

function BaseScene:mousepressed(x, y, button)
end

function BaseScene:mousereleased(x, y, button)
end

function BaseScene:mousemoved(x, y, dx, dy)
end

function BaseScene:keypressed(key, scancode, isrepeat)
end