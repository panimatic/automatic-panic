require 'scenes/base_scene'

TitleScene = class(BaseScene, function(instance)
	if BaseScene.init then BaseScene.init(instance) end

	instance.titleText = "Automatic Panic"	
	instance.keyText = "Press any key to start"
	instance.titleFont = love.graphics.newFont("assets/fonts/Stencilia-Bold.ttf", 55)
	instance.keyFont = love.graphics.newFont("assets/fonts/Stencilia-Bold.ttf", 35)
	instance.titleWidth = instance.titleFont:getWidth(instance.titleText)
	instance.keyWidth = instance.keyFont:getWidth(instance.keyText)

	instance.shaderTimer = 0
	local shaderCode, size = love.filesystem.read("assets/shaders/ripples.glsl")
	instance.shader = love.graphics.newShader(shaderCode)
end)

function TitleScene:draw()
	local width, height = love.graphics.getDimensions()
	love.graphics.setShader(self.shader)
    love.graphics.rectangle('fill', 0,0,width, height)
    love.graphics.setShader()

    love.graphics.setFont(self.titleFont)
    love.graphics.print(self.titleText, width / 2 - self.titleWidth / 2, 80)

	love.graphics.setFont(self.keyFont)
    love.graphics.print(self.keyText, width / 2 - self.keyWidth / 2, height - 120)
end

function TitleScene:update(dt)
	local width, height = love.graphics.getDimensions()
	self.shaderTimer = self.shaderTimer + dt
    self.shader:send("time", self.shaderTimer)
    self.shader:send("resolution", {width, height})
    self.shader:send("mouse", {love.mouse.getX(), love.mouse.getY()})
end

function TitleScene:keypressed(key, scancode, isrepeat)
	sceneManager:setScene(GameScene())
end