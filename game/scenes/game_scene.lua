require 'scenes/base_scene'
require 'objects/tower'
require 'objects/tower_generator'
require 'objects/map'
require 'objects/enemy'
require 'collections/towers'
require 'collections/tower_generators'
require 'components/map_path'
require 'collections/obstacles'
require 'components/collision_detector'
require 'components/towers_observer'
require 'objects/game_hud'

GameScene = class(BaseScene, function(self)
	self.towers = Towers()
	self.towerGenerators = TowerGenerators(self.towers)

	local width, height = love.graphics.getDimensions();

	self.map = Map("maps/map.json", width, height - 200)
	
	self.gameHud = GameHUD(self.map)

	self.obstacles = Obstacles()

	local towerGenerator = TowerGenerator({
			x = 400,
			y = 450,
			height = 20,
			width = 20,
			color = {140, 255, 216, 200},
			obstacles = self.obstacles.objects,
	});

	local tower = Tower({
		height = 160,
		width = 160,
		physicalHeight = 40,
		physicalWidth = 40,
		color = {140,255,216,200},
		range = 80,
		isBeingPressed = false,
		obstacles = self.obstacles.objects
	})

	tower:setCenteredPosition(500, 240)
	self.towerGenerators:add(towerGenerator)
	self.towers:add(tower)
	self.map:addObject(self.towerGenerators)
	self.map:addObject(self.towers)
	self.map:addObject(self.obstacles)

	self.enemyGroup = ObjectGroup()
	self.map:addObject(self.enemyGroup)

	self:addEnemies()

	self.towersObserver = new TowersObserver({
		towers = self.towers,
		enemies = self.enemyGroup,
		towerGenerators = self.towerGenerators
	})

	self.mapScrollX = 0
	self.mapScrollY = 0
end)

function GameScene:addEnemies()
	local mapPath = self.map.mapPath
	local endNode = self.map.endNode

	testNodes = {}

	table.insert(testNodes, {
			x = 0,
			y = 17
	})

	-- table.insert(testNodes, {
	-- 		x = 7,
	-- 		y = 2
	-- })

	for i = 1, 50, 1 do
		node = self.map.mapPath.nodeList[math.random(1, #self.map.mapPath.nodeList)]
		-- node = testNodes[math.random(1, #testNodes)]


		if node.value ~= 2 then
			local enemy = Enemy({
				x = node.x  * self.map.tileSize,
				y = node.y * self.map.tileSize,
				moveSpeed = 50 + math.round(50 * math.random()),
				height = 40,
				width = 40,
				color = {140,255,216,200},
				isBeingPressed = false,
				mapX = node.x,
				mapY = node.y,
				tileSize = self.map.tileSize,
			})

			enemy:setPath(mapPath:getPathFromXY(enemy.mapX, enemy.mapY, endNode))

			self.enemyGroup:add(enemy)
		end
	end
end

function GameScene:draw()
	self.map:draw()
	self.gameHud:draw()
end

function GameScene:update(dt)
	self.map:update(dt)
	self.gameHud:update(dt)


	local mX = 0
	local mY = 0

	if love.keyboard.isDown("up") then
		mY = -1
	end

	if love.keyboard.isDown("down") then
		mY = 1
	end

	if love.keyboard.isDown("left") then
		mX = -1
	end

	if love.keyboard.isDown("right") then
		mX = 1
	end

	if love.keyboard.isDown("r") then
		self.enemyGroup:clear()
		self:addEnemies()
	end


	if love.keyboard.isDown("d") then
		for _, enemy in ipairs(self.enemyGroup.objects) do
			enemy.timeDirection = -1
			enemy.isMoveAllowed = true
		end
 	end

	if love.keyboard.isDown("f") then
		for _, enemy in ipairs(self.enemyGroup.objects) do
			enemy.timeDirection = 1
			enemy.isMoveAllowed = true
		end
 	end

	self.mapScrollX = self.mapScrollX + mX
	self.mapScrollY = self.mapScrollY + mY

	self.map:updateViewport(self.mapScrollX, self.mapScrollY)
end

function GameScene:mousepressed(x, y, button)
	self.map:processMousePressed(x, y, button)	
end

function GameScene:mousereleased(x, y, button)
	self.map:processMouseReleased(x, y, button)	
end

function GameScene:mousemoved(x, y, dx, dy)
	self.map:processMouseMove(x, y, dx, dy)	
end