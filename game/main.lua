require 'components/scene_manager'
require 'scenes/all'
require 'components/vector'
require 'components/polygon'


sceneManager = nil

function love.load()
	sceneManager = SceneManager(GameScene())
	sceneManager:load()
end

function love.update(dt)
	sceneManager:update(dt)
end

function love.draw()
	sceneManager:draw()
	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.print("FPS: " .. tostring(love.timer.getFPS()), 10, 10)
end

function love.mousepressed(x, y, button)
	sceneManager:mousepressed(x, y, button)
end

function love.mousereleased(x, y, button)
	sceneManager:mousereleased(x, y, button)
end

function love.keypressed(key, scancode, isrepeat)
	sceneManager:keypressed(key, scancode, isrepeat)
end

function love.mousemoved(x, y, dx, dy)
	sceneManager:mousemoved(x, y, dx, dy)
end