/*
This software is in the public domain. Where that dedication is not recognized,
you are granted a perpetual, irrevokable license to copy and modify this file
as you see fit.
*/

// Blinn-Phong lighting shader for a single point light.

// Undefine this if you use love 0.9.1's sRGB conversion capabilities.
#define SHADER_GAMMA

#ifdef SHADER_GAMMA
const vec3 gamma = vec3(2.2);
const vec3 gammainverse = 1.0 / gamma;
#endif

// normalized eye direction, always the same because this is 2D.
const vec3 eyedir = vec3(0.0, 0.0, 1.0);


struct Material
{
	Image normalmap;
	Image specularmap;
	float shininess; // specular power, 0-255.
};

struct PointLight
{
	vec3 position; // position in screen space.
	vec3 color; // color values can be > 1.
	float radius; // radius in pixels in screen space.
};


extern vec3 ambientcolor;
extern PointLight light;
extern Material material;


// "varying" variables are written to in the vertex shader and the interpolated result is read in the pixel shader.
varying vec3 screenpos;


// When using a single file/string for both the vertex and pixel shader,
// parts specific to each must be incased around ifdefs or else errors will occur.
#ifdef VERTEX

vec4 position(mat4 transform_projection, vec4 vertex)
{
	// Get current vertex in screen-space.
	// Send the position in screen-space to the pixel shader.
	screenpos = vec3(TransformMatrix * vertex);
	
	// Return position in 'clip space' by transforming the current vertex using the Transform-Projection matrix.
	// This is what happens normally if no vertex shader is active.
	return transform_projection * vertex;
}

#endif // VERTEX


#ifdef PIXEL

vec4 effect(vec4 vcolor, Image texture, vec2 texcoord, vec2 pixcoord)
{
	vec4 finalcolor = vec4(ambientcolor * vcolor.rgb, 0.0);
	
	vec4 texcolor = Texel(texture, texcoord);
	
#ifdef SHADER_GAMMA
	// convert texture color from gamma-space to linear RGB. Math (lighting, etc.) should always be done in
	// linear RGB space, but texture colors are usually stored in gamma-space (sRGB).
	// If we use love 0.9.1, we can use its built-in sRGB/gamma-correction functionality so we don't have to do this manually.
	texcolor.rgb = pow(texcolor.rgb, gamma);
#endif

	finalcolor.rgb *= texcolor.rgb; // ambient diffuse color.
	
	// Calculate light direction and distance relative to the current position in screen-space.
	vec3 lightdir = (light.position - screenpos) / light.radius;
	
	// light intensity value based on distance from current pixel to light position.
	float lightintensity = max(1.0 - dot(lightdir, lightdir), 0.0);
	lightintensity *= lightintensity;
	
	if (lightintensity > 0.0)
	{
		vec3 normal = Texel(material.normalmap, texcoord).rgb;
		
		// NOTE: since NormalMatrix is a uniform value, it only works properly with directly drawn images/canvases/quads,
		// or *unrotated* sprites within spritebatches or particle systems.
		// Only interpolated per-vertex tangent values would work properly in all cases, and love doesn't give us that.
		normal = normalize(NormalMatrix * (normal * 2.0 - 1.0));
		
		// Diffuse contribution.
		vec3 Ldir = normalize(lightdir);
		float diffuseamount = max(dot(normal, Ldir), 0.0);
		vec3 diffuse = vcolor.rgb * texcolor.rgb * diffuseamount;
		
		// Specular contribution.
		vec3 specularcolor = Texel(material.specularmap, texcoord).rgb;
		float specularbase = max(dot(normal, normalize(Ldir + eyedir)), 0.0);
		float specularamount = pow(specularbase, material.shininess);
		vec3 specular = specularcolor * specularamount;
		
		finalcolor.rgb += lightintensity * light.color * (diffuse + specular);
	}
	
#ifdef SHADER_GAMMA
	finalcolor.rgb = pow(finalcolor.rgb, gammainverse);
#endif

	finalcolor.a = texcolor.a * vcolor.a;
		
	return finalcolor;
}

#endif // PIXEL
