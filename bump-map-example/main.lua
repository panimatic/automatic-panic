--[[
This software is in the public domain. Where that dedication is not recognized,
you are granted a perpetual, irrevokable license to copy and modify this file
as you see fit.
]]

function love.load()	
	material = {
		color = love.graphics.newImage("cross_color.png"),
		normalmap = love.graphics.newImage("cross_norm.png"),
		specularmap = love.graphics.newImage("cross_spec.png"),
	}
	
	imagenames = {}
	for k in pairs(material) do
		table.insert(imagenames, k)
		if k == "color" then -- material.color is the default image to be drawn
			imageindex = #imagenames
		end
	end
	
	shader = love.graphics.newShader("bumpmap.glsl")
	
	shader:send("material.normalmap", material.normalmap)
	shader:send("material.specularmap", material.specularmap)
	shader:send("material.shininess", 50) -- specular power in the range of 0-255
	
	local lightintensity = 1.5
	local lightcolor = {255/255, 255/255, 255/255}
	for i,v in ipairs(lightcolor) do
		lightcolor[i] = v * lightintensity
	end
	
	shader:send("light.color", lightcolor)
	shader:send("light.radius", 200)
	
	shader:send("ambientcolor", {(16/255)^2.2, (16/255)^2.2, (16/255)^2.2})
	
	lightpos = {0, 0, 0}
	
	useshader = true
end

function love.keypressed(key)
	if key == "escape" then
		love.event.quit()
	elseif key == "t" or key == " " then
		useshader = not useshader
	elseif key == "up" or key == "right" then
		imageindex = ((imageindex + 1 - 1) % (#imagenames)) + 1
	elseif key == "down" or key == "left" then
		imageindex = ((imageindex - 1 - 1) % (#imagenames)) + 1
	end
end

function love.update(dt)
	lightpos = {love.mouse.getX(), love.mouse.getY(), 60}
end

function love.draw()	
	if useshader then
		love.graphics.setShader(shader)
		shader:send("light.position", lightpos) -- light position is in screen space
	end
	
	local image = material[imagenames[imageindex]]
	local xres, yres = love.graphics.getWidth(), love.graphics.getHeight()
	local iw, ih = image:getWidth(), image:getHeight()
	
	love.graphics.draw(image, xres/2, yres/2, love.timer.getTime()*0.1, 1, 1, iw/2, ih/2)
	
	if useshader then
		love.graphics.setShader()
	end
	
	local fps = love.timer.getFPS()
	local ms = 1000*love.timer.getAverageDelta()
	local str = ("%.2f ms (%d fps)\nimage: %s\n%s"):format(ms, fps, imagenames[imageindex], useshader and "shader on" or "shader off")
	love.graphics.print(str, 2, 2)
end
